alert("Hello World");

// PERSONAL INFO

let firstName="Louie Mark";
console.log("First Name: " + firstName);

let lastName="Zaragoza";
console.log( "Last Name: " + lastName);

let myAge=27;
console.log("Age: " + myAge);

let hobbies=["Photography", "Gardening", "Listening to Music"];
console.log("Hobbies: ")
console.log(hobbies);

let workAddress={
	houseNumber: "962",
	street: "Aurora Boulevard",
	city: "Quezon City",
	isActive: false
}
console.log("Work Address: ")
console.log(workAddress);

let space="\n\n"
console.log(space);


// DEBUGGING SECTION

	let fullName = "Steve Rogers";
	console.log("My full name is: " + fullName);

	let age = 40;
	console.log("My current age is: " + age);
	
	let friends = ["Tony","Bruce", "Thor","Natasha","Clint","Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: ")
	console.log(profile);

	let fullName1 = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName1);

	const lastLocation = "Arctic Ocean";
	
	console.log("I was found frozen in: " + lastLocation);


